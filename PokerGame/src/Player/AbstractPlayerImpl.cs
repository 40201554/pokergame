﻿using PokerGame.src.Cards;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokerGame.src.Player
{
    abstract class AbstractPlayerImpl : Player
    {
        private List<Card> cards;

        private int money;

        PlayerImpl()
        {
            
        }

        public int getMoney()
        {
            return this.money;
        }

        public void recieveCards(List<Card> cardsRecieved)
        {
            cards.AddRange(cardsRecieved);
        }

        public int bid(int amount)
        {
            money -= amount; 
            return amount;
        }

        public int raise(int amount)
        {
            money -= amount;
            return amount;
        }

        public void win (int amount)
        {
            money += amount;
        }

        public void check()
        {

        }

        public int allIn()
        {
            int amount = money;
            money = 0;
            return amount;
        }
    }
}
