﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokerGame.src.Player
{
    interface Player
    {
        int bid(int amount);
        int raise(int amount);
        void check();
        void win(int ammount);
        int allIn();
    }
}
