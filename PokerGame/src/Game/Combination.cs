﻿enum Combination
{
    HighCard = 1,
    Pair = 2,
    TwoPairs = 6,
    Trio = 14,
    Straight = 17,
    Flush = 18,
    FullHouse = 19,
    SmallRoyal = 20,
    Caree = 21,
    BigRoyal = 23
}