﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokerGame.src.Cards
{
    class Card
    {
        private Suit suit;

        private Sign sign;

        public Card(Suit suit, Sign sign)
        {
            this.suit = suit;
            this.sign = sign;
        }
   
        public Suit getSuit()
        {
            return this.suit;
        }

        public Sign getSign()
        {
            return this.sign;
        }
    }
}
