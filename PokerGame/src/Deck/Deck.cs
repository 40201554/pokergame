﻿using PokerGame.src.Cards;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace PokerGame.src.Deck
{
    class Deck
    {
        private List<Card> cards = new List<Card>();

        public Deck()
        {
                foreach (Sign sign in Enum.GetValues(typeof(Sign)))
                {
                    foreach (Suit suit in Enum.GetValues(typeof(Suit)))
                    {
                        cards.Add(new Card(suit, sign));
                    }
                }
        }

        public Card drawCard()
        {
            Random rn = new Random();
            Card card;
            card = cards[rn.Next(cards.Count)];
            cards.Remove(card);
            return card;
        }
    }
}
